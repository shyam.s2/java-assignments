package org.java;

import java.util.Scanner;
 
class Palindrome {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("Enter a word to check it is palindrome or not");
        String S= scanner.nextLine();
       
        if(palndrm(S))
        {
            System.out.println(S+" is a palindrome");
        }
        else
        {
            System.out.println(S+" is not a palindrome");
        }
    }
 
    public static boolean palndrm(String S) {
        int left = 0, right = S.length() - 1;
        
        while(left < right)
        {
            if(S.charAt(left) != S.charAt(right))
            {
                return false;
            }
            left++;
            right--;
        }
        return true;
    } 
}
