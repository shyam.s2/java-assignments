package org.java;

public class Substring {
	
    public static void main(String[] args) {
        String sentence = "The quick brown fox jumps over the lazy dog.";
        String sub = sentence.substring(4, 10);
        System.out.println("Original sentence: " + sentence);
        System.out.println("Substring: " + sub);
    }

}