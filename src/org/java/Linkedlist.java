package org.java;

import java.util.LinkedList;

public class Linkedlist {

    public static void main(String[] args) {
        LinkedList<String> list = new LinkedList<>();

        list.add("Honda");
        list.add("Tata");
        list.add("Maruti");
        list.add("Benz");
       

        System.out.println("LinkedList: " + list);

        list.addFirst("Hyundai");

        System.out.println("LinkedList after adding 'Hyundai': " + list);

        list.remove("Benz");

        System.out.println("LinkedList after removing 'Benz': " + list);

        int size = list.size();
        System.out.println("Size of LinkedList: " + size);

        boolean contains = list.contains("Tata");
        System.out.println("LinkedList contains 'Tata': " + contains);

        for (String item : list) {
            System.out.println(item);
        }
    }
}