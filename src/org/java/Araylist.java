package org.java;

import java.util.ArrayList;

public class Araylist {

    public static void main(String[] args) {
        
        ArrayList<String> list = new ArrayList<>();

        
        list.add("Honda");
        list.add("Tata");
        list.add("Maruti");
        list.add("Benz");

        
        System.out.println("ArrayList: " + list);

        
        list.remove("Benz");

        
        System.out.println("ArrayList after removing 'Benz': " + list);

        int size = list.size();
        System.out.println("Size of ArrayList: " + size);

        boolean contains = list.contains("Tata");
        System.out.println("ArrayList contains 'Tata': " + contains);

        for (String item : list) {
            System.out.println(item);
        }
    }
}